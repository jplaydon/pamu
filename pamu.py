#!/usr/bin/python

import ply.lex as lex
import ply.yacc as yacc
from ply.lex import TOKEN

import re
import sys
import os.path
from time import strftime

#     ##### #   # ##### ####
#     #      # #  #     #   #
#     ###     #   ###   ####
#     #      # #  #     #  #
##### ##### #   # ##### #   #

tokens = (
    'MULTILINECOMMENT',
    'ONELINECOMMENT',
    'STRING',
    'FLOAT',
    'INT',
    'ID',
)

# Operators

def intbools(bool):
    return 1 if bool else 0

binops = {
    '*' : ('TIMES', lambda a, b: a * b,),
    '/' : ('DIVIDE', lambda a, b: a / b,),
    '%' : ('MOD', lambda a, b: a % b,),
    '+' : ('PLUS', lambda a, b: a + b,),
    '-' : ('MINUS', lambda a, b: a - b,),
    '<<' : ('LSHFT', lambda a, b: a << b,),
    '>>' : ('RSHFT', lambda a, b: a << b,),
    '<' : ('LT', lambda a, b: intbools(a < b),),
    '>' : ('GT', lambda a, b: intbools(a > b),),
    '<=' : ('LE', lambda a, b: intbools(a <= b),),
    '>=' : ('GE', lambda a, b: intbools(a >= b),),
    '=' : ('EQ', lambda a, b: intbools(a == b),),
    '!=' : ('NE', lambda a, b: intbools(a != b),),
    '&' : ('BAND', lambda a, b: a & b,),
    '^' : ('BXOR', lambda a, b: intbools(a != b),),
    '&&' : ('AND', lambda a, b: intbools(a and b),),
    '||' : ('OR', lambda a, b: a | b,),
}
#t_BINOP = '[' + (''.join([binops[k][1] for k in binops])) + ']'
for binop in binops:
    globals()['t_' + binops[binop][0]] = re.escape(binop)
tokens += tuple(binops[binop][0] for binop in binops if binops[binop][0] not in tokens)

unops = {
    '!' : ('NOT', lambda a: 0 if a != 0 else 1,),
    '~' : ('BNOT', lambda a: ~a,),
    '-' : ('MINUS', lambda a: -a),
}
#t_UNOP = '[' + (''.join([unops[k][0] for k in unops])) + ']'
for unop in unops:
    globals()['t_' + unops[unop][0]] = re.escape(unop)
tokens += tuple(unops[unop][0] for unop in unops if unops[unop][0] not in tokens)

precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'BXOR'),
    ('left', 'BAND'),
    ('left', 'EQ', 'NE'),
    ('nonassoc', 'LT', 'GT', 'LE', 'GE'),
    ('left', 'LSHFT', 'RSHFT'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MOD'),
    ('right', 'NOT', 'BNOT', 'UNARY_OP'),
)

def t_MULTILINECOMMENT(t):
    r'\#\>(.|\s)*?\<\#'
    pass

def t_ONELINECOMMENT(t):
    r'\#.*$'
    pass

def t_FLOAT(t):
    r'\d+\.\d+'
    t.value = float(t.value)
    return t

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

# Strings (must use " quotes)
t_STRING = r'"(\\"|[^"])*"'

# Identifiers
t_ID = r'[a-zA-Z_][0-9a-zA-Z_]*'

# Other syntactical things
pamu_tokens = {
    'DEFINE': r'$', # to introduce a new name (immutable)
    'COLON': r':', # to mark the end of a pattern in the pattern matching bit
    'FULLSTOP': r'.',
    'COMMA': r',',
    'LAMBDASTART': r'{',
    'LAMBDAEND': r'}',
    'TUPLESTART': r'(',
    'TUPLEEND': r')',
    'INDEXSTART': r'[',
    'INDEXEND': r']',
    'GUARDSEP': '\\',
    'RESTSEP': r'|', # used in tuple expressions, if c was (3, 4) then (1, 2 | c) would be (1, 2, 3, 4)
    'MATCH': r'?',
    'CLOSURESELF': r'@',
}
for token in pamu_tokens:
    globals()['t_' + token] = re.escape(pamu_tokens[token])
tokens += tuple(token for token in pamu_tokens if token not in tokens)

# Define a rule so we can track line numbers (from the PLY example)
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

t_ignore = ' \t'

# Error handling rule
def t_error(t):
    print "Illegal character '%s'" % t.value[0]
    # (don't) DROP IT

lexer = lex.lex(reflags=re.MULTILINE)

####   ###  ####   ###  ##### ####
#   # #   # #   # #     #     #   #
####  ##### ####   ###  ###   ####
#     #   # #  #      # #     #  #
#     #   # #   #  ###  ##### #   #

def p_goal(t):
    """goal : statement_list"""
    t[0] = t[1]

def p_error(t):
    print "Parsing error! '%s'" % t

def p_statement_list(t):
    """statement_list : statement FULLSTOP
                      | statement_list statement FULLSTOP"""
    if len(t) == 3: # output, statement, fullstop
        t[0] = (t[1],)
    elif len(t) == 4: # output, statement, fullstop, stmtlist
        t[0] = t[1] + (t[2],)

def p_statement(t):
    """statement : definition
                 | call"""
    t[0] = t[1]

def p_definition(t):
    """definition : DEFINE assignable_id expression
                  | DEFINE tuple expression"""
    #t[0] = ("DEFINE", t[2], t[3],)
    t[0] = DefineStatement(t[2], t[3])

def p_expression(t):
    """expression : id
                  | tuple
                  | call
                  | index_expression
                  | slice_expression
                  | binary_op
                  | unary_op
                  | string
                  | number
                  | lambda"""
    t[0] = t[1]

def p_expression_list(t):
    """expression_list : expression
                       | expression_list COMMA expression"""
    if len(t) == 2:
        t[0] = (t[1],)
    elif len(t) == 4:
        t[0] = t[1] + (t[3],)

def p_assignable_id(t):
    """assignable_id : ID"""
    #t[0] = ("ID", t[1],)
    t[0] = Id(t[1])

def p_closure_self(t):
    """closure_self : CLOSURESELF"""
    t[0] = Id(t[1])

def p_id(t):
    """id : assignable_id
          | closure_self"""
    #t[0] = ("ID", t[1],)
    t[0] = t[1]

def p_tuple_inner(t):
    """tuple_inner :
                   | expression_list
                   | RESTSEP expression
                   | expression_list RESTSEP expression"""
    if len(t) == 1:
        #t[0] = ("TUPLE", (), (),)
        t[0] = TupleExpr((), ())
    elif len(t) == 2:
        #t[0] = ("TUPLE", t[1], (),)
        t[0] = TupleExpr(t[1], ())
    elif len(t) == 3:
        #t[0] = ("TUPLE", (), t[2],)
        t[0] = TupleExpr((), t[2])
    elif len(t) == 4:
        #t[0] = ("TUPLE", t[1], t[3],)
        t[0] = TupleExpr(t[1], t[3])

def p_tuple(t):
    """tuple : TUPLESTART tuple_inner TUPLEEND"""
    t[0] = t[2]

def p_callable_expression(t):
    """callable_expression : id
                           | call
                           | index_expression
                           | slice_expression
                           | lambda"""
    # not number, string, binary_op, tuple
    t[0] = t[1]

def p_call(t):
    """call : callable_expression tuple
            | callable_expression MATCH tuple"""
    #t[0] = ("CALL", t[1], t[2],)
    if len(t) == 3:
        t[0] = Call(t[1], t[2])
    elif len(t) == 4:
        t[0] = Call(t[1], t[3], True) # Match expression

def p_indexable_expression(t):
    """indexable_expression : assignable_id
                            | call
                            | index_expression
                            | slice_expression
                            | string
                            | tuple"""
    t[0] = t[1]

def p_index_expression(t):
    """index_expression : indexable_expression INDEXSTART expression INDEXEND"""
    t[0] = Index(t[1], t[3])

def p_slice_expression(t):
    """slice_expression : indexable_expression INDEXSTART COLON INDEXEND
                        | indexable_expression INDEXSTART COLON expression INDEXEND
                        | indexable_expression INDEXSTART expression COLON INDEXEND
                        | indexable_expression INDEXSTART expression COLON expression INDEXEND"""
    if len(t) == 5:
        t[0] = Slice(t[1], None, None)
    elif len(t) == 6:
        if t[3] == pamu_tokens['COLON']:
            t[0] = Slice(t[1], None, t[4])
        else:
            t[0] = Slice(t[1], t[3], None)
    elif len(t) == 7:
        t[0] = Slice(t[1], t[3], t[5])

def p_binary_op(t):
    """binary_op : expression """
    #t[0] = ("BINOP", t[1], t[2], t[3],)
    t[0] = BinOp(t[2], t[1], t[3])
p_binary_op.__doc__ += (' expression\n             | expression '.join([binops[op][0] for op in binops])) + " expression"

def p_unary_op(t):
    """unary_op : """
    #t[0] = ("UNOP", t[1], t[2],)
    t[0] = UnOp(t[1], t[2])
p_unary_op.__doc__ += (' expression %prec UNARY_OP\n             | '.join([unops[op][0] for op in unops])) + " expression %prec UNARY_OP"

def p_string(t):
    """string : STRING"""
    #t[0] = ("STRING", t[1],)
    t[0] = String(parsestring(t[1]))

def p_number(t):
    """number : INT
              | FLOAT"""
    #t[0] = ("NUMBER", t[1],)
    t[0] = Number(t[1])

def p_lambda(t):
    """lambda : LAMBDASTART LAMBDAEND
              | LAMBDASTART patterncase_list LAMBDAEND"""
    if len(t) == 3:
        #t[0] = ("LAMBDA", (),)
        t[0] = Lambda(()) # such a lambda would fail if you tried to call it
    elif len(t) == 4:
        #t[0] = ("LAMBDA", t[2],)
        t[0] = Lambda(t[2])

def p_patterncase_list(t):
    """patterncase_list : patterncase
                        | patterncase_list patterncase"""
    if len(t) == 2:
        t[0] = (t[1],)
    elif len(t) == 3:
        t[0] = t[1] + (t[2],)

def p_comma_statement_list(t):
    """comma_statement_list : statement
                            | comma_statement_list COMMA statement"""
    if len(t) == 2:
        t[0] = (t[1],)
    elif len(t) == 4:
        t[0] = t[1] + (t[3],)

def p_patterncase_statement_list(t):
    """patterncase_statement_list : expression FULLSTOP
                                  | comma_statement_list COMMA expression FULLSTOP"""
    if len(t) == 3: # output, statement, fullstop
        t[0] = (t[1],)
    elif len(t) == 5: # output, statement, comma, stmtlist
        t[0] = t[1] + (t[3],)

def p_patterncase(t):
    """patterncase : pattern COLON patterncase_statement_list"""
    #t[0] = t[1] + (t[3],)
    t[0] = PatternCase(t[1][0], t[1][1], t[3])

def p_pattern(t):
    """pattern : tuple_inner
               | tuple_inner GUARDSEP expression_list"""
    if len(t) == 2:
        t[0] = (t[1], (),)
    elif len(t) == 4:
        t[0] = (t[1], t[3],)

parser = yacc.yacc()

##### #   # ##### ##### ####  ####  ####  ##### ##### ##### ####
  #   ##  #   #   #     #   # #   # #   # #       #   #     #   #
  #   # # #   #   ###   ####  ####  ####  ###     #   ###   ####
  #   #  ##   #   #     #   # #     #   # #       #   #     #   #
##### #   #   #   ##### #   # #     #   # #####   #   ##### #   #

class PamuError(Exception):
    def __repr__(self):
        return self.__str__()

class PamuImmutableError(PamuError):
    def __init__(self, id, newvalue, oldvalue):
        PamuError.__init__(self)
        self.id = id
        self.newvalue = newvalue
        self.oldvalue = oldvalue

    def __str__(self):
        return "Immutability error: Tried to bind '%s' to '%s', but it was already bound to '%s'." % (self.id, self.newvalue, self.oldvalue,)

class PamuIdNotFoundError(PamuError):
    def __init__(self, id, availableids):
        PamuError.__init__(self)
        self.id = id
        self.availableids = availableids
        
    def __str__(self):
        return "ID not found error: Tried to look up ID '%s', but it was not found in the current environment. (Bound IDs: %s)" % (self.id, str(self.availableids),)

class PamuNoPatternMatchesError(PamuError):
    def __init__(self, args):
        PamuError.__init__(self)
        self.args = args

    def __str__(self):
        return "No Pattern Matches error: No match found in the patterns (and guards) in the closure for '%s'. TODO: Make this error message more useful!" % (self.args,)

class PamuNotIndexableError(PamuError):
    def __init__(self):
        PamuError.__init__(self)

    def __str__(self):
        return "Not Indexable error: Cannot index this! TODO: Make this error message more useful!"

class PamuIndexOutOfBoundsError(PamuError):
    def __init__(self, length, index):
        PamuError.__init__(self)
        self.length = length
        self.index = index

    def __str__(self):
        return "Index Out Of Bounds error: Tried to do thing[%d] but thing was only %d long! TODO: Make this error message more useful!" % (self.index, self.length,)

class PamuUnificationError(PamuError):
    def __init__(self, value):
        PamuError.__init__(self)
        self.value = str(value)

    def __str__(self):
        return "Unification Error: Could not unify expression with '%s'. TODO: Make this error message more useful!" % (self.value,)

class PamuEnv(object):
    def __init__(self, parentenv):
        self.locals = {}
        self.parentenv = parentenv

    def bind(self, id, value):
        if id == r'_':
            pass
        elif id in self.locals:
            raise PamuImmutableError(id, value, self.locals[id])
        else:
            self.locals[id] = value

    def read(self, id):
        #print "Trying to read '%s'" % id
        if id in self.locals:
            return self.locals[id]
        elif id == r'_':
            raise PamuIdNotFoundError(id, self.allids())
        elif self.parentenv == None:
            raise PamuIdNotFoundError(id, self.allids())
        else:
            return self.parentenv.read(id)

    def allids(self):
        return self.locals.keys() + ([] if self.parentenv == None else self.parentenv.allids())

class DefineStatement(object):
    def __init__(self, dest, expr):
        self.dest = dest
        self.expr = expr

    def eval(self, env):
        value = self.expr.eval(env)
        if not self.dest.unify(env, value):
            raise PamuUnificationError(value)
        return value

class Expr(object):
    def __init__(self):
        self.iscall = False
        self.istailcall = False
        self.indexable = False

    # { 1 + 2:prints("should work").}(3).
    def unify(self, env, value):
        return self.eval(env) == value

    def index(self, env, index):
        if not self.indexable:
            raise PamuNotIndexableError()
        value = self.eval(env)
        if len(value) <= index:
            raise PamuIndexOutOfBoundsError(len(value), index)
        return value[index]

    def slice(self, env, slice_start, slice_length=None):
        if not self.indexable:
            raise PamuNotIndexableError()
        value = self.eval(env)
        if not slice_length:
            slice_length = len(value) - slice_start
        #if len(value) < (slice_start + slice_length):
        #    raise PamuIndexOutOfBoundsError(len(value), slice_start + slice_length)
        return value[slice_start:slice_length]

class Id(Expr):
    def __init__(self, name):
        Expr.__init__(self)
        self.name = name
        self.indexable = True

    def eval(self, env):
        return env.read(self.name)

    def unify(self, env, value):
        env.bind(self.name, value)
        return True

class Number(Expr):
    def __init__(self, value):
        Expr.__init__(self)
        self.value = value

    def eval(self, env):
        return self.value


class UnOp(Expr):
    def __init__(self, opname, expr):
        Expr.__init__(self)
        self.op = unops[opname][1] # get the op from the global dictionary of unops
        self.expr = expr

    def eval(self, env):
        return self.op(self.expr.eval(env))

class BinOp(Expr):
    def __init__(self, opname, expr1, expr2):
        Expr.__init__(self)
        self.op = binops[opname][1] # get the op from the global dictionary of binops
        self.expr1 = expr1
        self.expr2 = expr2

    def eval(self, env):
        return self.op(self.expr1.eval(env), self.expr2.eval(env))

ps_subs = {r'n' : '\n', r't' : '\t'}
ps_charre = r'(\\[\\"' + ''.join(ps_subs.keys()) + ']|[^\\"])'
ps_stringrec = re.compile(r'^"(' + ps_charre + '*)"$')
ps_charrec = re.compile(ps_charre)
def parsestring(stringstring):
    content = ps_stringrec.match(stringstring).group(1) # get the inner content from the string
    return ''.join(map(lambda c: c if len(c) == 1 else (c[1] if c[1] not in ps_subs else ps_subs[c[1]]), ps_charrec.findall(content)))

class String(Expr):
    def __init__(self, value):
        Expr.__init__(self)
        self.value = value
        self.indexable = True

    def eval(self, env):
        return self.value

class TupleExpr(Expr):
    def __init__(self, startexprs, restexpr):
        Expr.__init__(self)
        self.startexprs = startexprs
        self.restexpr = restexpr
        self.indexable = True
    
    def unify(self, env, value):
        if len(value) < len(self.startexprs):
            return False
        for i in range(len(self.startexprs)):
            if not self.startexprs[i].unify(env, value[i]):
                return False
        if self.restexpr:
            return self.restexpr.unify(env, value[len(self.startexprs):])
        else:
            return len(value) == len(self.startexprs)
    
    def eval(self, env):
        return tuple(expr.eval(env) for expr in self.startexprs) + (() if not self.restexpr else self.restexpr.eval(env))

class Index(Expr):
    def __init__(self, expr, indexexpr):
        Expr.__init__(self)
        self.expr = expr
        self.indexexpr = indexexpr
        self.indexable = True
    
    def eval(self, env):
        index = self.indexexpr.eval(env)
        return self.expr.index(env, index)

class Slice(Expr):
    def __init__(self, expr, slice_start_expr, slice_length_expr):
        Expr.__init__(self)
        self.expr = expr
        self.slice_start_expr = slice_start_expr
        self.slice_length_expr = slice_length_expr
        self.indexable = True
    
    def eval(self, env):
        slice_start = 0 if not self.slice_start_expr else self.slice_start_expr.eval(env)
        slice_length = None if not self.slice_length_expr else self.slice_length_expr.eval(env)
        return self.expr.slice(env, slice_start, slice_length)

class Call(Expr):
    def __init__(self, closureexpr, argexpr, match=False):
        Expr.__init__(self)
        self.closureexpr = closureexpr
        self.argexpr = argexpr
        self.iscall = True
        self.indexable = True
        self.match = match

    def eval(self, env):
        closure = self.closureexpr.eval(env)
        args = self.argexpr.eval(env)
        if not self.match:
            (isexpr, clenv, expr,) = closure.call(args)
            while isexpr and expr.istailcall:
                (isexpr, clenv, expr,) = expr.call()
            if isexpr:
                return expr.eval(clenv)
            else:
                return expr
        else:
            return intbools(closure.match(args))

class TailCall(object):
    def __init__(self, call, env):
        self.closure = call.closureexpr.eval(env)
        self.args = call.argexpr.eval(env)
        self.istailcall = True

    def call(self):
        return self.closure.call(self.args)

class PatternCase(object):
    def __init__(self, patternexpr, guardexprs, execexprs):
        self.patternexpr = patternexpr
        self.guardexprs = guardexprs
        self.execexprs = execexprs

    def unify(self, env, args):
        unified = self.patternexpr.unify(env, args)
        if unified:
            for guardexpr in self.guardexprs: # if the pattern matches, check the guard expressions
                if guardexpr.eval(env) == 0:
                    return False
            return True
        else:
            return False

    def eval(self, env):
        for i in range(len(self.execexprs) - 1): # eval all but the last
            self.execexprs[i].eval(env)
        if len(self.execexprs) > 0: # return the last instead for tail call optimisation
            expr = self.execexprs[-1]
            if expr.iscall: # turn the Call object into a TailCall object so it will return the correct (env, expr,) tuple and also have the correct env
                return (True, env, TailCall(expr, env),)
            else:
                return (True, env, expr,) # so that expr.eval(env) works
        else:
            return None

class Lambda(Expr):
    def __init__(self, patterncases):
        Expr.__init__(self)
        self.patterncases = patterncases

    def eval(self, env):
        return Closure(env, self)

class Closure(object):
    def __init__(self, env, lmbda):
        self.env = env
        self.lmbda = lmbda

    def call(self, args):
        for pc in self.lmbda.patterncases:
            newenv = PamuEnv(self.env)
            newenv.bind(pamu_tokens['CLOSURESELF'], self)
            if pc.unify(newenv, args):
                return pc.eval(newenv)
        else:
            raise PamuNoPatternMatchesError(args)

    def match(self, args):
        for pc in self.lmbda.patterncases:
            newenv = PamuEnv(self.env)
            newenv.bind(pamu_tokens['CLOSURESELF'], self)
            if pc.unify(newenv, args):
                return True
        else:
            return False

def returnn(n):
    return (False, None, n,) # enclosed in a tuple for the tail call optimization code (which expects closure calls to return things of this format)

def returns(s):
    return (False, None, s,)

def returnt(t):
    return (False, None, s,)

def returnc(c):
    return (False, None, c,)

class BuiltInClosure(object):
    pass

class BuiltInPrintString(BuiltInClosure):
    def call(self, args):
        sys.stdout.write(''.join(args)),
        return returnn(1)
    
    def match(self, args):
        return True

class BuiltInSingleArgClosure(BuiltInClosure):
    def __init__(self, closure):
        BuiltInClosure.__init__(self)
        self.closure = closure

    def call(self, args):
        if self.match(args):
            return self.closure(args[0])
        else:
            raise PamuNoPatternMatchesError(args)

    def match(self, args):
        return len(args) == 1

class BuiltInFile(BuiltInClosure):
    def __init__(self, file):
        BuiltInClosure.__init__(self)
        self.file = file
        self.mode = self.file.mode

    def call(self, args):
        if self.match(args):
            if len(args) == 1:
                if args[0] == "c":
                    self.file.close()
                    return returnn(0)
                elif args[0] == "n":
                    return returns(self.file.name)
            elif len(args) == 2:
                if args[0] == "r":
                    if args[1] == "a":
                        return returns(self.file.read())
                    elif args[1] == "l":
                        return returns(self.file.readline())
                elif args[0] == "w":
                    self.file.write(args[1])
                    return returnn(0)
        else:
            raise PamuNoPatternMatchesError(args)

    def match(self, args):
        if len(args) == 1:
            return args[0] in ("c", "n")
        elif len(args) == 2:
            if args[0] == "r":
                return isinstance(args[1], int) or args[1] in ("a", "l")
            elif args[0] == "w":
                return isinstance(args[1], str)
        return False
                

class BuiltInFileOpen(BuiltInClosure):
    def call(self, args):
        if self.match(args):
            try:
                file = None
                if len(args) == 1:
                    file = open(args[0])
                elif len(args) == 2:
                    file = open(args[0], args[1])
                return returnc(BuiltInFile(file))
            except IOError as e:
                return returnt((e.errno, e.strerror))
        else:
            raise PamuNoPatternMatchesError(args)

    def match(self, args):
        return len(args) in (1, 2)
    

single_arg_closures = {
    'numtostr' : lambda v: returns(str(v)),
    'len' : lambda v: returnn(len(v)),
    'strftime' : lambda v: returns(strftime(v)),
    'isfile' : lambda v: returnn(intbools(os.path.isfile(v))),
    'isdir' : lambda v: returnn(intbools(os.path.isdir(v))),
}

def topLevelPamuEnv(argv=()):
    env = PamuEnv(None)
    # Add argv
    env.bind('argv', tuple(str(arg) for arg in argv))
    # Standard streams
    env.bind('stdout', BuiltInFile(sys.stdout))
    env.bind('stdin', BuiltInFile(sys.stdin))
    env.bind('stderr', BuiltInFile(sys.stderr))
    # Add built-in single arg closures
    for sac_name in single_arg_closures:
        env.bind(sac_name, BuiltInSingleArgClosure(single_arg_closures[sac_name]))
    # Add other built-ins.
    env.bind('prints', BuiltInPrintString())
    env.bind('openfile', BuiltInFileOpen())
    # Add some functions written in Pamu itself.
    pamuExecString(r'$printn {n: prints(numtostr(n)).}.', env)
    pamuExecString(r'$car {x | _: x.}.', env)
    pamuExecString(r'$cdr {_ | xs: xs.}.', env)
    pamuExecString(r'$cons {x | xs: (x | xs).}.', env)
    pamuExecString(r'$id {v: v.}.', env)
    pamuExecString(r'$curry {f | args1: {| args2: f(| args1 + args2).}.}.', env)
    # In Python: curry = lambda f, *args1: lambda *args2: f(*(args1 + args2))
    
    return env

def pamuExecString(code, env=None, argv=()):
    if not env:
        env = topLevelPamuEnv(argv=argv)
    for stmt in parser.parse(code):
        stmt.eval(env)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        code = None
        with open(sys.argv[1], 'r') as code_file:
            code = code_file.read()
        pamuExecString(code, argv=tuple(sys.argv[1:]))
