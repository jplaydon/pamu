# README #

Pamu is a new programming language designed by myself as an experiment. Nothing is fixed and things can and will change.

At the time of writing, it has the following features:

 * Immutability.
 * First-class functions (closures).
 * Anonymous functions.
   * Recursive anonymous functions.
 * Strings, tuples and indexing and slicing them.
 * Pattern matching on function call arguments.
 * Using integers as booleans.
 * A slightly strange syntax.

Possible future features include:

 * REPL (would be written in Python, not Pamu since Pamu will not have the ability to eval itself).
 * All strings to be UTF-8.
 * Remotely helpful error messages.
 * Become typed.
   * Static type-checking before the program is run.
   * The ability to specify which types things should be to help the static type checker, and cause error messages if it's being used incorrectly.
 * Compile to LLVM Intermediate Representation, which could then be compiled by LLVM to output native executables.
   * Probably requires static typing to make it more feasible.

### How do I get set up? ###

 * Pamu is being developed on Windows using CPython 2.7.8 (other platforms and Python versions may vary).
   * (Though it doesn't use too many special Python features, a lot of Pamu behaviour is dependent on Python behaviour, for example whether you can add two things together is entirely dependent on whether Python thinks you can.)
 * The only non-standard Python library Pamu uses is [Python Lex-Yacc](http://www.dabeaz.com/ply/), for lexing and parsing, naturally.
 * Run "python pamu.py program.pamu" to execute program.pamu.
   * the # character is the start of a rest-of-the-line comment, so you can use #! paths on Unix-like systems.