def init(): #immute
    set_msg_hndlr(f(0))

def f(msgs): # immute
    def hndlr(msg): # immute
        nmsgs = msgs + 1
        print "msg #" + str(nmsgs) + ": " + msg
        set_msg_hndlr(f(nmsgs))
    return hndlr

# Mutable state

msg_hndlr = None

def set_msg_hndlr(new_msg_hndlr):
    global msg_hndlr
    msg_hndlr = new_msg_hndlr
